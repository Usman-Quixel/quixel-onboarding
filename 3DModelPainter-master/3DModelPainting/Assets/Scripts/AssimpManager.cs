﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleFileBrowser;
using Assimp.Unmanaged;

public class AssimpManager : MonoBehaviour
{
    public Material material;

    private Assimp.Scene m_model;
    private Vector3 m_sceneCenter, m_sceneMin, m_sceneMax;
    private float m_angle;
    private int m_displayList;
    private int m_texId;
    private Mesh mesh;
    private MeshFilter mf;

    public void loadMeshButtonHandel()
    {
        ShowFileLoadBrowser();
    }
    void ShowFileLoadBrowser()
    {
        FileBrowser.SetFilters(true, new FileBrowser.Filter("Mesh file", ".dae" , ".fbx" , ".obj" , ".blend"));
        FileBrowser.SetDefaultFilter(".dae");
        FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
        FileBrowser.AddQuickLink("CurrentProject", @"C:\Projects\Unity\Personal\quixel-onboarding\3DModelPainter-master", null);

        StartCoroutine(ShowLoadDialogCoroutine());
    }

    IEnumerator ShowLoadDialogCoroutine()
    {
        yield return FileBrowser.WaitForLoadDialog(false, @"D:\Projects\Unity\Personal\Repo\quixel-onboarding\3DModelPainter-master\3DModelPainting", "Load Mesh", "Load");
        if(FileBrowser.Success)
            LoadMesh(FileBrowser.Result);
        Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);
    }

    void LoadMesh(System.String fileName)
    {
        Debug.Log(fileName);
       // UnmanagedLibrary library = AssimpLibrary.Instance;

        //Debug.Log("Current AssImp Version = "+Assimp.Unmanaged.AssimpLibrary.Instance.GetVersion());
        Assimp.AssimpContext importer = new Assimp.AssimpContext();
        importer.SetConfig(new Assimp.Configs.NormalSmoothingAngleConfig(66.0f));
        importer.SetConfig(new Assimp.Configs.MeshVertexLimitConfig(60000));
        importer.SetConfig(new Assimp.Configs.MeshTriangleLimitConfig(60000));
        importer.SetConfig(new Assimp.Configs.RemoveDegeneratePrimitivesConfig(true));
        importer.SetConfig(new Assimp.Configs.SortByPrimitiveTypeConfig(Assimp.PrimitiveType.Line | Assimp.PrimitiveType.Point));

        Assimp.PostProcessSteps postProcessSteps = Assimp.PostProcessPreset.TargetRealTimeMaximumQuality | Assimp.PostProcessSteps.MakeLeftHanded | Assimp.PostProcessSteps.FlipWindingOrder;

        m_model = importer.ImportFile(fileName, postProcessSteps);
        mesh = ConvertASSimpToUnity(m_model.Meshes[0]);
        if (gameObject.GetComponent<MeshRenderer>() == null)
        {
            MeshRenderer mr = gameObject.AddComponent<MeshRenderer>();
            mr.material = new Material(material);
            mf = gameObject.AddComponent<MeshFilter>();
        }
        mf.mesh = this.mesh;
    }

    Mesh ConvertASSimpToUnity(Assimp.Mesh importedMesh)
    {
        Mesh mesh = new Mesh();
        mesh.vertices = ConvertASSimpToUnity(importedMesh.Vertices.ToArray());
        mesh.normals = ConvertASSimpToUnity(importedMesh.Normals.ToArray());
        //mesh.uv = ConvertASSimpToUnity(importedMesh..ToArray());
        mesh.triangles = importedMesh.GetIndices();
        mesh.RecalculateBounds();
        
        return mesh;
    }
    Vector3[] ConvertASSimpToUnity(Assimp.Vector3D[] vector3DIn)
    {
        Vector3[] vectorArrayOut = new Vector3[vector3DIn.Length];
        for (int i = 0; i < vector3DIn.Length; i++)
        {
            vectorArrayOut[i].x = vector3DIn[i].X;
            vectorArrayOut[i].y = vector3DIn[i].Y;
            vectorArrayOut[i].z = vector3DIn[i].Z;
        }
        return vectorArrayOut;
    }
    int[] ConvertASSimpToUnity(Assimp.Face[] faceIn)
    {
        int[] tris = new int[faceIn.Length * 3];
        for (int i = 0; i < faceIn.Length; i++)
        {
            tris[i] = faceIn[i].Indices.IndexOf(0);
            tris[i+1] = faceIn[i].Indices.IndexOf(1);
            tris[i+2] = faceIn[i].Indices.IndexOf(2);
        }
        return tris;
    }
  
}
