Shader "TessellationExperiments/Testellate" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Offset ("Offset", Float) = 10
		_Tessellation ("Tessellation", Float) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		
		CGPROGRAM
		#pragma surface surf BlinnPhong vertex:displace tessellate:fixedTessellation nolightmap
		#pragma target 5.0
		#pragma debug
		
		sampler2D _MainTex;
		float _Offset;
		float _Tessellation;

		
		
		float fixedTessellation(){
			return _Tessellation;		
		}
		
		struct appdata {
	        float4 vertex : POSITION;
//		    float4 tangent : TANGENT;
		    float3 normal : NORMAL;
		    float4 texcoord : TEXCOORD0;
//		    float4 texcoord1 : TEXCOORD1;
	    };

		struct Input {
			float2 uv_MainTex;
		};
		
		void displace(inout appdata v){
//			UNITY_INITIALIZE_OUTPUT(Input,o);// Input Initialization required for DirectX11
		
			// Calculate mesh coordinates, 
			// Can't use TRANSFORM_TEX
			// since _HeightTex_ST does not seem to be available yet at this point in the shader
			// And setting  _HeightTex_ST gives problems when it is used later.
			// Does not actually change the texture coordinates.
			float4 customUVTransform = float4(1,1,0,0);
			float2 uv = v.texcoord.xy * customUVTransform.xy + customUVTransform.zw;
		
			float localTex = tex2Dlod(_MainTex, float4(uv,0,0)).r;
			
			v.vertex.y += localTex.r * _Offset;
//			v.normal = calcGridNormal(_HeightTex, uv, _HeightTex_TexelSize.xy, float3(1, _Offset, 1), localTex.r);// Store normal for lighting calculations
		}


		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
		}
		ENDCG
	}
}