using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class DebugInfo : MonoBehaviour {
	
	public Rect debugInfoBounds = new Rect(10,10,250,500);
	public GUISkin debugInfoSkin;
	public GUIStyle unsupportedFeatureStyle;
	
	void OnGUI(){
		GUISkin originalSkin = GUI.skin;
		GUI.skin = debugInfoSkin;
		
		GUILayout.BeginArea(debugInfoBounds);
		
		GUILayout.Label("Device Type: " + SystemInfo.deviceType);
		GUILayout.Label(SystemInfo.operatingSystem);			
		GUILayout.Label("Unity Version: " + Application.unityVersion);
		GUILayout.Label(SystemInfo.graphicsDeviceVersion);
		GUILayout.Label(SystemInfo.graphicsDeviceName);
		GUILayout.Label("Supported Shader Level: " + SystemInfo.graphicsShaderLevel);
		GUILayout.Label("Graphics Device ID: " + SystemInfo.graphicsDeviceID);
		GUILayout.Label("Graphics Memory Size: " + SystemInfo.graphicsMemorySize);
		
		#if UNITY_4_1
		GUILayout.Label("Maximum Texture Size: " + SystemInfo.maxTextureSize);
		GUILayout.Label("Non-Power-Of-Two Texture Support: " + SystemInfo.npotSupport);
		#endif
		
		displaySupport("ComputeShaders", SystemInfo.supportsComputeShaders);
		displaySupport("RenderTextures", SystemInfo.supportsRenderTextures);
		displaySupport("3DTextures", SystemInfo.supports3DTextures);
		
		displaySupport("ARGBFloat", SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBFloat));
		displaySupport("Depth Textures", SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.Depth));
		
		GUILayout.EndArea();
		
		
		GUI.skin = originalSkin;
	}
	
	private void displaySupport(string name, bool supported){
		if (supported){
			GUILayout.Label(name + " Supported");	
		}
		else{
			GUILayout.Label(name + " Not Supported", unsupportedFeatureStyle);	
		}
	}
	
	
}
