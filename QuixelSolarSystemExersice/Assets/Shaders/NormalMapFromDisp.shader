﻿Shader "Quixel/NormalMapFromDisp"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DispMap ("Displacment Map", 2D) = "white" {}
		_normalStrength("Normal Strength" , float) = 8.0
       // _NormalMap ("NormalMap", 2D) = "bump" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float4 tangent : TANGENT;
				float3 normal : NORMAL;
				float4 uv : TEXCOORD0;
				
			};

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float3 N : TEXCOORD1; // normal world space
				float3 T : TEXCOORD2; // Tangent world space
				float3 B : TEXCOORD3; // BiTangent worls space
				float4 V : TEXCOORD4; // Position in world space
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _DispMap;
            sampler2D _NormalMap;
            float4 _MainTex_ST;
            float4 _DispMap_ST;
            float4 _DispMap_TexelSize;
            float4 _NormalMap_ST;
			uniform float4 _LightColor0;
			float _normalStrength;

            v2f vert (appdata v)
            {
                v2f o;
				//v.vertex = mul(_Matrix, v.vertex);
				//v.normal = mul((float3x3)_Matrix, v.normal);
				//v.tangent = float4(mul((float3x3)_Matrix, v.tangent), v.tangent.w);

                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.N = normalize(mul(unity_ObjectToWorld, float4(v.normal.xyz, 0.0)).xyz);
				o.T = normalize(mul(unity_ObjectToWorld, float4(v.tangent.xyz, 0.0)).xyz);

				o.B = normalize(cross(o.N, o.T) * v.tangent.w);
				o.V = mul(unity_ObjectToWorld, v.vertex);

                return o;
            }
			

			float4 ComputeNormalsPS(in float2 uv:TEXCOORD0 , sampler2D displacementSampler) : COLOR
			{

				//http://www.catalinzima.com/2008/01/converting-displacement-maps-into-normal-maps/
				//https://en.wikipedia.org/wiki/Sobel_operator
				float textureSize = _DispMap_TexelSize.w;
				float texelSize = 1.0f / textureSize; //size of one texel;

				float tl = abs(tex2D(displacementSampler, uv + texelSize * float2(-1, -1)).x);   // top left
				float  l = abs(tex2D(displacementSampler, uv + texelSize * float2(-1,  0)).x);   // left
				float bl = abs(tex2D(displacementSampler, uv + texelSize * float2(-1,  1)).x);   // bottom left
				float  t = abs(tex2D(displacementSampler, uv + texelSize * float2(0, -1)).x);   // top
				float  b = abs(tex2D(displacementSampler, uv + texelSize * float2(0,  1)).x);   // bottom
				float tr = abs(tex2D(displacementSampler, uv + texelSize * float2(1, -1)).x);   // top right
				float  r = abs(tex2D(displacementSampler, uv + texelSize * float2(1,  0)).x);   // right
				float br = abs(tex2D(displacementSampler, uv + texelSize * float2(1,  1)).x);   // bottom right

				// Compute dx using Sobel:
				//           -1 0 1 
				//           -2 0 2
				//           -1 0 1
				float dX = tr + 2 * r + br - tl - 2 * l - bl;

				// Compute dy using Sobel:
				//           -1 -2 -1 
				//            0  0  0
				//            1  2  1
				float dY = bl + 2 * b + br - tl - 2 * t - tr;

				// Build the normalized normal
				//float4 N = float4(normalize(float3(dX, 1.0f / normalStrength, dY)), 1.0f);
				float4 N = float4(normalize(float3(-dX, -dY, 1.0f / _normalStrength)), 1.0f);

				//convert (-1.0 , 1.0) to (0.0 , 1.0), if needed
				return N *0.5f + 0.5f;
			}

            fixed4 frag (v2f input) : SV_Target
            {
				 //float4 encodedNormal = tex2D(_NormalMap,_NormalMap_ST.xy * input.uv.xy + _NormalMap_ST.zw);
				 float4 encodedNormal = ComputeNormalsPS(input.uv.xy, _DispMap);
				 /*float3 localCoords = float3(2.0 * encodedNormal.a - 1.0 ,2.0 * encodedNormal.g - 1.0, 0.0);
				 localCoords.z = sqrt(1.0 - dot(localCoords, localCoords));*/

				 float3 localCoords = float3(2.0 * encodedNormal.r - 1.0, 2.0 * encodedNormal.g - 1.0, 2.0 * encodedNormal.b - 1.0);

				 float4 normalDebug = float4 (0,0,0,0);

				 normalDebug.x = localCoords.r;
				 normalDebug.y = localCoords.g;
				 normalDebug.z = localCoords.b;

				 float3x3 local2WorldTranspose = float3x3(
					input.T,
					input.B,
					input.N);

				// float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));
				 float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));

				 float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				 float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb;

				 float4 color = tex2D(_MainTex, input.uv);
				 float nl = max(0.0, dot(normalDirection, lightDirection));
				 float3 diffuseReflection = _LightColor0.rgb * nl;

				 //return normalDebug;
				 return  float4(ambientLighting + diffuseReflection, 1.0)* color;
            }
            ENDCG
        }
    }
}
