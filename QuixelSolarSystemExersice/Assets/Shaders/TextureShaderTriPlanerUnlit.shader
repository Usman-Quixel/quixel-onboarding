﻿
Shader "Quixel/FlatTextureTriPlaner"
{

    Properties
    {
        _MainTex("Main texture", 2D) = "white" {}
		_NormalMap("Normal Map", 2D) = "bump" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        CGPROGRAM

        #pragma surface surf Standard vertex:vert

        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _NormalMap;

        struct Input
        {
            float3 localCoord;
            float3 localNormal;
        };

        void vert(inout appdata_full v, out Input data)
        {
            data.localCoord = v.vertex.xyz;
            data.localNormal = v.normal.xyz;
        }

        void surf(Input IN, inout SurfaceOutputStandard o)
        {
            // Blending factor of triplanar mapping
            float3 bf = normalize(abs(IN.localNormal));
            bf /= dot(bf, (float3)1);

            // Triplanar mapping
            float2 tx = IN.localCoord.yz ;
            float2 ty = IN.localCoord.zx ;
            float2 tz = IN.localCoord.xy ;

            // Base color
            half4 cx = tex2D(_MainTex, tx) * bf.x;
            half4 cy = tex2D(_MainTex, ty) * bf.y;
            half4 cz = tex2D(_MainTex, tz) * bf.z;

            half4 color = (cx + cy + cz);
            o.Albedo = color.rgb;
            o.Alpha = color.a;

			half4 nx = tex2D(_NormalMap, tx) * bf.x;
			half4 ny = tex2D(_NormalMap, ty) * bf.y;
			half4 nz = tex2D(_NormalMap, tz) * bf.z;
			o.Normal = UnpackScaleNormal(nx + ny + nz, 1);

        }
        ENDCG
    }
    FallBack "Diffuse"
}
