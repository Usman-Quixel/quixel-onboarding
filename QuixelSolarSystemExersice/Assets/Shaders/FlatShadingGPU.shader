﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Quixel/FlatShadingGPU"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		/*_Rotation ("Rotation Axis" , Vector) = (0,0,0,0)
		_Pivot("pivot" , Vector) = (0,0,0,0)
		_ParentPos("_ParentPos" , Vector) = (0,0,0,0)*/
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _Rotation;
			float4 _Pivot;
			uniform float4x4 _ParentTRSMatrix;
			uniform float4x4 _currentTRSMatrix;

			float4x4 ZRotationMatrix(float degrees)
			{
				float alpha = degrees * UNITY_PI / 180.0 * _Time.y;
				float sina, cosa;
				sincos(alpha, sina, cosa);
				return float4x4(
					cosa, -sina, 0,0,
					sina, cosa , 0,0,
					0 ,0 ,1 ,0,
					0, 0, 0, 1);
			}
			float4x4 XRotationMatrix(float degrees)
			{
				float alpha = degrees * UNITY_PI / 180.0 * _Time.y;
				float sina, cosa;
				sincos(alpha, sina, cosa);
				return float4x4(
					1, 0, 0,0,
					0, cosa, -sina ,0,
					0, sina, cosa ,0,
					0,0,0,1);
			}
			float4x4 YRotationMatrix(float degrees)
			{
				float alpha = degrees * UNITY_PI / 180.0 * _Time.y;
				float sina, cosa;
				sincos(alpha, sina, cosa);
				return float4x4(
					cosa, 0, -sina,0,
					0, 1, 0,0,
					sina, 0, cosa,0,
					0, 0, 0, 1);
			}

			v2f vert(appdata v)
			{
				float4 pos = v.vertex;
				pos += _Pivot;
				// set the translation matrix.
				float4x4 translateMatrix = float4x4(1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					0, 0, 0 , 1);

				// set yhe scale.
				float4x4 scaleMatrix = float4x4(1, 0, 0, 0,
					0, 1, 0, 0,
					0, 0, 1, 0,
					0, 0, 0, 1);
				
				//merge all the TRS With vertpostion.
				float4x4 finalTRS = mul(translateMatrix, _ParentTRSMatrix);
				finalTRS = mul(finalTRS, scaleMatrix);
				finalTRS = mul(finalTRS, XRotationMatrix(_Rotation.x));
				finalTRS = mul(finalTRS, YRotationMatrix(_Rotation.y));
				finalTRS = mul(finalTRS, ZRotationMatrix(_Rotation.z));

				_currentTRSMatrix = finalTRS;

				float4 finalPosition = mul(_currentTRSMatrix, pos);
				//Done! now return the result
				v2f o;
				
				o.vertex = UnityObjectToClipPos(finalPosition);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o, o.vertex);
				return o;
			}

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
