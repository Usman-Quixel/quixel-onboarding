﻿Shader "Quixel/NormalMapMerge"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NormalMap ("NormalMapA", 2D) = "bump" {}
        _NormalMapB ("NormalMapB", 2D) = "bump" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			struct appdata 
			{
				float4 vertex : POSITION;
				float4 tangent : TANGENT;
				float3 normal : NORMAL;
				float4 uv : TEXCOORD0;
				
			};

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float3 N : TEXCOORD1; // normal world space
				float3 T : TEXCOORD2; // Tangent world space
				float3 B : TEXCOORD3; // BiTangent worls space
				float4 V : TEXCOORD4; // Position in world space
				float3 normal : TEXCOORD5; // normal world space
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _NormalMap;
            sampler2D _NormalMapB;
            float4 _MainTex_ST;
            float4 _NormalMap_ST;
            float4 _NormalMapB_ST;
			uniform float4 _LightColor0;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				//o.N = normalize(mul(unity_WorldToObject, float4(v.normal.xyz, 0.0)).xyz);
				o.N = normalize(mul(unity_ObjectToWorld, float4(v.normal.xyz, 0.0)).xyz);
				o.T = normalize(mul(unity_ObjectToWorld, float4(v.tangent.xyz, 0.0)).xyz);

				o.B = normalize(cross(o.N, o.T) * v.tangent.w);
				o.V = mul(unity_ObjectToWorld, v.vertex);

				o.normal = UnityObjectToWorldNormal(v.normal);

                return o;
            }

            fixed4 frag (v2f input) : SV_Target
            {
				 float4 encodedNormal = tex2D(_NormalMap,_NormalMap_ST.xy * input.uv.xy + _NormalMap_ST.zw);

				 float3 localCoords = float3(2.0 * encodedNormal.a - 1.0 ,2.0 * encodedNormal.g - 1.0, 0.0);
				 localCoords.z = sqrt(1.0 - dot(localCoords, localCoords));

				 encodedNormal = tex2D(_NormalMapB, _NormalMapB_ST.xy * input.uv.xy + _NormalMapB_ST.zw);
				 float3 localCoordsB = float3(2.0 * encodedNormal.a - 1.0, 2.0 * encodedNormal.g - 1.0, 0.0);
				 localCoordsB.z = sqrt(1.0 - dot(localCoordsB, localCoordsB));

				 float3x3 local2WorldTranspose = float3x3(
					input.T,
					input.B,
					input.N);

				 localCoords += localCoordsB;

				 localCoords = normalize(localCoords);

				 float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));

				 float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				 float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb;

				 float4 color = tex2D(_MainTex, input.uv);

				 //float normalMapP = max(0.0, dot(normalDirection, lightDirection));
				 float normalMapP = saturate(dot(normalDirection, lightDirection));;

				 float worldNormalP = max(0.0, dot(input.normal, lightDirection));

				 float3 diffuseReflection = _LightColor0.rgb * normalMapP;

				 return float4(ambientLighting + diffuseReflection , 1.0) * color;
            }
            ENDCG
        }
    }
}
