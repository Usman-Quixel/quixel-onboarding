﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Quixel/NormalMapObbjectSpace"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NormalMap ("NormalMap", 2D) = "bump" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 uv : TEXCOORD0;
				
			};

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _NormalMap;
            float4 _MainTex_ST;
            float4 _NormalMap_ST;
			uniform float4 _LightColor0;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f input) : SV_Target
            {
				 float3 normalDirection = normalize(mul(tex2D(_NormalMap, input.uv.xy).xyz * 2.0 - 1.0, (float3x3)unity_WorldToObject));

				 float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz); ;

				 float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb;

				 float3 diffuseReflection = _LightColor0.rgb * max(0.0, dot(normalDirection, lightDirection));

				 return float4(ambientLighting + diffuseReflection, 1.0);
            }
            ENDCG
        }
    }
}
