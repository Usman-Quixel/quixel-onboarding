﻿Shader "Quixel/PerPixelDisplacment"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NormalMap ("NormalMap", 2D) = "bump" {}
		_Displacement("Vector Displacement Map", 2D) = "white" {}
		_height("Height", Float) = 0.1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex distanceVertex
            #pragma fragment distanceFragment
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

			struct a2vConnector
			{
				float4 Coord : POSITION;
				float4 Tangent : TANGENT;
				float3 Normal : NORMAL;
				float4 texCoord : TEXCOORD0;
				
			};

            struct v2fConnector
            {
                float4 projCoord : SV_POSITION;
				float3 texCoord : TEXCOORD0; // normal world space
				float3 tanEyeVec : TEXCOORD1; // Tangent world space
				float3 tanLightVec : TEXCOORD2; // BiTangent worls space
				float3 N : TEXCOORD3; // normal world space
				float3 T : TEXCOORD4; // Tangent world space
				float3 B : TEXCOORD5; // BiTangent worls space
				float3 normal : TEXCOORD6; // normal world space
            };

			
            sampler2D _MainTex;
            sampler2D _NormalMap;
            sampler2D _Displacement;

            float4 _MainTex_ST;
            float4 _NormalMap_ST;
            float4 _Displacement_ST;


			float _height;

			uniform float4 _LightColor0;
			
			float2 TraceRay(in float height, in float2 coords, in float3 dir) {

				float2 NewCoords = coords;
				float2 dUV = -dir.xy * height * 0.08;
				float SearchHeight = 1.0;
				float prev_hits = 0.0;
				float hit_h = 0.0;
				int numItration = 10;
				for (int i = 0; i < numItration; i++) {
					SearchHeight -= numItration/100;
					NewCoords += dUV;
					float CurrentHeight = tex2Dlod(_Displacement, float4(NewCoords.xy,0,0)).r;
					float first_hit = clamp((CurrentHeight - SearchHeight - prev_hits) * 499999.0, 0.0, 1.0);
					hit_h += first_hit * SearchHeight;
					prev_hits += first_hit;
				}
				NewCoords = coords + dUV * (1.0 - hit_h) * 10.0f - dUV;

				float2 Temp = NewCoords;
				SearchHeight = hit_h + 0.1;
				float Start = SearchHeight;
				dUV *= 0.2;
				prev_hits = 0.0;
				hit_h = 0.0;
				for (int i = 0; i < numItration/2; i++) {
					SearchHeight -= numItration/5000;
					NewCoords += dUV;
					float CurrentHeight = tex2Dlod(_Displacement, float4(NewCoords.xy, 0, 0)).r;
					float first_hit = clamp((CurrentHeight - SearchHeight - prev_hits) * 499999.0, 0.0, 1.0);
					hit_h += first_hit * SearchHeight;
					prev_hits += first_hit;
				}
				NewCoords = Temp + dUV * (Start - hit_h) * 50.0f;

				return NewCoords;
			}

			v2fConnector distanceVertex(a2vConnector a2v)
			{
				v2fConnector v2f;
				// Project position into screen space
				// and pass through texture coordinate
				v2f.projCoord = UnityObjectToClipPos( a2v.Coord);
				v2f.texCoord = float3(a2v.texCoord.xy, 1);
				// Transform the eye vector into tangent space.
				// Adjust the slope in tangent space based on bump depth

				float3 objNormal = a2v.Normal;
				float4 objTangent = a2v.Tangent;
				float3 objBinormal = normalize(cross(objNormal, objTangent.xyz) * objTangent . w);
				float3 eyeVec = normalize(ObjSpaceViewDir(v2f.projCoord));
				float3 tanEyeVec;

				tanEyeVec.x = dot(objTangent, eyeVec);
				tanEyeVec.y = dot(objBinormal, eyeVec);
				tanEyeVec.z = dot(objNormal, eyeVec);
				v2f.tanEyeVec =  normalize(tanEyeVec);

				v2f.N = normalize(mul(unity_ObjectToWorld, float4(objNormal, 0.0)).xyz);
				v2f.T = normalize(mul(unity_ObjectToWorld, float4(objTangent.xyz, 0.0)).xyz);
				v2f.B = normalize(cross(v2f.N, v2f.T) * objTangent.w);
				v2f.normal = UnityObjectToWorldNormal(a2v.Normal);

				float3 lightVec = _WorldSpaceLightPos0;
				float3 tanLightVec;
				tanLightVec.x = dot(objTangent, lightVec);
				tanLightVec.y = dot(objBinormal, lightVec);
				tanLightVec.z = dot(objNormal, lightVec);
				v2f.tanLightVec = tanLightVec;
				return v2f;
			}

			float4 distanceFragment(v2fConnector v2f) : SV_TARGET
			{
				
				float3  fvLightDirection = normalize(v2f.tanLightVec);
				float3  fvViewDirection = normalize(v2f.tanEyeVec);

				float2 NewCoord = TraceRay(_height, v2f.texCoord, fvViewDirection);

				float4 encodedNormal = tex2D(_NormalMap, _NormalMap_ST.xy * NewCoord + _NormalMap_ST.zw);

				float3 localCoords = float3(2.0 * encodedNormal.a - 1.0, 2.0 * encodedNormal.g - 1.0, 0.0);
				localCoords.z = sqrt(1.0 - dot(localCoords, localCoords));

				float3x3 local2WorldTranspose = float3x3(
					v2f.T,
					v2f.B,
					v2f.N);

				float3 normalDirection = normalize(mul(localCoords, local2WorldTranspose));

				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

				float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb;

				float4 color = tex2D(_MainTex, NewCoord);

				//float normalMapP = max(0.0, dot(normalDirection, lightDirection));
				float normalMapP = saturate(dot(normalDirection, lightDirection));;

				float worldNormalP = max(0.0, dot(v2f.normal, lightDirection));

				float3 diffuseReflection = _LightColor0.rgb * normalMapP;

				return float4(ambientLighting + diffuseReflection, 1.0) * color;
				
			}
            ENDCG
        }
    }
}
