﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float speed = 2;

    float angle;
    // Update is called once per frame
    void Update()
    {
        angle += Time.deltaTime * speed;
        transform.rotation = Quaternion.Euler(0, angle, 0);
    }
}
