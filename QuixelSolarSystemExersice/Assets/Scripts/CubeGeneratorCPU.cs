﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum CubeType {none, simple , dedicatedSides}

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class CubeGeneratorCPU : MonoBehaviour
{

    public Vector3 translation = Vector3.zero;
    public Vector3 rotationSpeed = Vector3.zero;
    public Vector3 OrbitrotationSpeed = Vector3.zero;
    public Vector3 scale = Vector3.one;
    public CubeType cubeToGenerate = CubeType.none;
    public bool isVisible = true;
    public Material material;
    public CubeGeneratorCPU target;

    public bool animateTranslation = false;
    public TranslationAnimation translationAnimation;

    [HideInInspector]
    public Matrix4x4 m;

    Shader shader;
    MeshRenderer meshRenderer;
    Quaternion rot;
    Quaternion orbitRot;
    void Start()
    {
        init();

    }
    void init()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        //shader = Shader.Find("Quixel/FlatShadingCPU");
        shader = Shader.Find("Quixel/SolarSystem");
        //shader = Shader.Find("Quixel/FlatShadingCPU_withNormalMap");
        //shader = Shader.Find("Quixel/Triplanar");
        if (cubeToGenerate == CubeType.simple)
            CreateSimpleCube();
        else if (cubeToGenerate == CubeType.dedicatedSides)
            CreateCube();
    }
    private void OnValidate()
    {
        if (Application.isPlaying)
        {
            init();
        }
    }

    private void CreateSimpleCube()
    {
        float minPoint = -0.5f;
        float maxPoint = 0.5f;

        Vector3 up = Vector3.up;
        Vector3 down = Vector3.down;
        Vector3 front = Vector3.back;
        Vector3 back = Vector3.forward;
        Vector3 left = Vector3.left;
        Vector3 right = Vector3.right;

        Vector3[] vertices = {

            new Vector3(minPoint, maxPoint, minPoint),
            new Vector3(minPoint, minPoint, minPoint),
            new Vector3(maxPoint, maxPoint, minPoint),
            new Vector3(maxPoint, minPoint, minPoint),

            new Vector3(minPoint, minPoint, maxPoint),
            new Vector3(maxPoint, minPoint, maxPoint),
            new Vector3(minPoint, maxPoint, maxPoint),
            new Vector3(maxPoint, maxPoint, maxPoint),

            new Vector3(minPoint, maxPoint, minPoint),
            new Vector3(maxPoint, maxPoint, minPoint),
            new Vector3(minPoint, maxPoint, minPoint),
            new Vector3(minPoint, maxPoint, maxPoint),

            new Vector3(maxPoint, maxPoint, minPoint),
            new Vector3(maxPoint, maxPoint, maxPoint),
        };


        Vector2[] uvs = new Vector2[]
        {
            new Vector2(0, 0.66f),
            new Vector2(0.25f, 0.66f),
            new Vector2(0, 0.33f),
            new Vector2(0.25f, 0.33f),

            new Vector2(0.5f, 0.66f),
            new Vector2(0.5f, 0.33f),
            new Vector2(0.75f, 0.66f),
            new Vector2(0.75f, 0.33f),

            new Vector2(1, 0.66f),
            new Vector2(1, 0.33f),

            new Vector2(0.25f, 1),
            new Vector2(0.5f, 1),

            new Vector2(0.25f, 0),
            new Vector2(0.5f, 0),
        };

        int[] triangles = {
            0, 2, 1, // front
			1, 2, 3,
            4, 5, 6, // back
			5, 7, 6,
            6, 7, 8, //top
			7, 9 ,8,
            1, 3, 4, //bottom
			3, 5, 4,
            1, 11,10,// left
			1, 4, 11,
            3, 12, 5,//right
			5, 12, 13

        };

        Vector3[] normales = {
            ((front+up+left)/3).normalized,   // 0
            ((front+left+down)/3).normalized, // 1
            ((front+right+up)/3).normalized,  // 2
            ((front+right+down)/3).normalized, // 3
            ((back+left+down)/3).normalized,  // 4
            ((back+right+down)/3).normalized,  // 5
            ((back+left+up)/3).normalized,  // 6
            ((back+right+up)/3).normalized, //7
            ((front+up+left)/3).normalized, // 8
            ((front+right+up)/3).normalized,// 9
            ((front+up+left)/3).normalized, // 10
            ((back+left+up)/3).normalized, // 11
            ((front+right+up)/3).normalized, // 12
            ((back+right+up)/3).normalized // 13
        };
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.uv = uvs;
        mesh.triangles = triangles;
        mesh.normals = normales;

        mesh.RecalculateTangents();
        mesh.RecalculateBounds();

        if(material == null)
            material = new Material(shader);
        meshRenderer.material = material;
    }
    private void CreateCube()
    {
        float length = 1f;
        float width = 1f;
        float height = 1f;

        Vector3 p0 = new Vector3(-length * .5f, -width * .5f, height * .5f);
        Vector3 p1 = new Vector3(length * .5f, -width * .5f, height * .5f);
        Vector3 p2 = new Vector3(length * .5f, -width * .5f, -height * .5f);
        Vector3 p3 = new Vector3(-length * .5f, -width * .5f, -height * .5f);

        Vector3 p4 = new Vector3(-length * .5f, width * .5f, height * .5f);
        Vector3 p5 = new Vector3(length * .5f, width * .5f, height * .5f);
        Vector3 p6 = new Vector3(length * .5f, width * .5f, -height * .5f);
        Vector3 p7 = new Vector3(-length * .5f, width * .5f, -height * .5f);

        Vector3[] vertices = new Vector3[]
        {
	        // Bottom
	        p0, p1, p2, p3,
 
	        // Left
	        p7, p4, p0, p3,
 
	        // Front
	        p4, p5, p1, p0,
 
	        // Back
	        p6, p7, p3, p2,
 
	        // Right
	        p5, p6, p2, p1,
 
	        // Top
	        p7, p6, p5, p4
        };

        Vector3 up = Vector3.up;
        Vector3 down = Vector3.down;
        Vector3 front = Vector3.forward;
        Vector3 back = Vector3.back;
        Vector3 left = Vector3.left;
        Vector3 right = Vector3.right;

        Vector3[] normales = new Vector3[]
        {
	        // Bottom
	        down, down, down, down,
 
	        // Left
	        left, left, left, left,
 
	        // Front
	        front, front, front, front,
 
	        // Back
	        back, back, back, back,
 
	        // Right
	        right, right, right, right,
 
	        // Top
	        up, up, up, up
        };

        Vector2 _00 = new Vector2(0f, 0f);
        Vector2 _10 = new Vector2(1f, 0f);
        Vector2 _01 = new Vector2(0f, 1f);
        Vector2 _11 = new Vector2(1f, 1f);

        Vector2[] uvs = new Vector2[]
        {
	        // Bottom
	        _11, _01, _00, _10,
 
	        // Left
	        _11, _01, _00, _10,
 
	        // Front
	        _11, _01, _00, _10,
 
	        // Back
	        _11, _01, _00, _10,
 
	        // Right
	        _11, _01, _00, _10,
 
	        // Top
	        _11, _01, _00, _10,
        };

        int[] triangles = new int[]
         {
	        // Bottom
	        3, 1, 0,
            3, 2, 1,			
 
	        // Left
	        3 + 4 * 1, 1 + 4 * 1, 0 + 4 * 1,
            3 + 4 * 1, 2 + 4 * 1, 1 + 4 * 1,
 
	        // Front
	        3 + 4 * 2, 1 + 4 * 2, 0 + 4 * 2,
            3 + 4 * 2, 2 + 4 * 2, 1 + 4 * 2,
 
	        // Back
	        3 + 4 * 3, 1 + 4 * 3, 0 + 4 * 3,
            3 + 4 * 3, 2 + 4 * 3, 1 + 4 * 3,
 
	        // Right
	        3 + 4 * 4, 1 + 4 * 4, 0 + 4 * 4,
            3 + 4 * 4, 2 + 4 * 4, 1 + 4 * 4,
 
	        // Top
	        3 + 4 * 5, 1 + 4 * 5, 0 + 4 * 5,
            3 + 4 * 5, 2 + 4 * 5, 1 + 4 * 5,

        };


        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.normals = normales;
        mesh.uv = uvs;
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();

        if (material == null)
            material = new Material(shader);
        meshRenderer.material = material;
        if (!isVisible)
        {
            meshRenderer.enabled = false;
        }

    }

    private void LateUpdate()
    {
        Vector3 ObjectPostion = translation;
        if (animateTranslation)
        {

            ObjectPostion = translationAnimation.GetNewPostion(translation);
        }
        rot = Quaternion.Euler(rotationSpeed.x * Time.time, rotationSpeed.y * Time.time, rotationSpeed.z * Time.time);
        if (target)
        {
            orbitRot = Quaternion.Euler(OrbitrotationSpeed.x * Time.time, OrbitrotationSpeed.y * Time.time, OrbitrotationSpeed.z * Time.time);
            Matrix4x4 orbitTRS = Matrix4x4.TRS(target.m.GetColumn(3), orbitRot, Vector3.one);
            m = Matrix4x4.TRS(orbitTRS.MultiplyPoint3x4(ObjectPostion), rot, scale);
        }
        else
            m = Matrix4x4.TRS(ObjectPostion, rot, scale);
        meshRenderer.material.SetMatrix("_Matrix", m);
    }
    private void OnDrawGizmos()
    {
        if (target)
        {
            UnityEditor.Handles.color = Color.white;
            UnityEditor.Handles.DrawWireDisc(target.m.GetColumn(3), Vector3.Normalize(Vector3.up), translation.x);
            //Gizmos.DrawWireSphere(target.m.GetColumn(3), translation.x);
        }
    }

}
[System.Serializable]
public class TranslationAnimation
{
    public Vector3 offset;
    public float speed;
    float time;
    public Vector3 GetNewPostion(Vector3 origin)
    {
        time +=  Time.deltaTime * speed;
        return Vector3.Lerp(origin - offset, origin + offset, Mathf.PingPong(time, 1));
    }

}
