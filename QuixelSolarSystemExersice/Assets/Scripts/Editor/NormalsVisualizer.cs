﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MeshFilter))]
public class NormalsVisualizer : Editor
{

    private Mesh mesh;

    void OnEnable()
    {
        MeshFilter mf = target as MeshFilter;
        if (mf != null)
        {
            mesh = mf.sharedMesh;
        }
    }

    void OnSceneGUI()
    {
        if (mesh == null)
        {
            return;
        }
        if (mesh.vertexCount < 500)
        {
            for (int i = 0; i < mesh.vertexCount; i++)
            {
                Handles.matrix = (target as MeshFilter).transform.localToWorldMatrix;
                Handles.color = Color.yellow;
                Handles.DrawLine(
                    mesh.vertices[i],
                    mesh.vertices[i] + mesh.normals[i]);
                Handles.color = Color.magenta;
                Handles.DrawLine(
                    mesh.vertices[i],
                    mesh.vertices[i] + (Vector3)mesh.tangents[i]);
            }
        }
    }
}