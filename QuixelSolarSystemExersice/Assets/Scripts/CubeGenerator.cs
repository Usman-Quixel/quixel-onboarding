﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent (typeof( MeshRenderer))]
[RequireComponent (typeof(MeshFilter))]
public class CubeGenerator : MonoBehaviour
{

    public Vector3 rotationSpeed = Vector3.zero;
    public Vector3 pivot = Vector3.zero;


    MeshRenderer meshRenderer;

    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        CreateCube();
    }

    private void CreateCube()
    {
        float minPoint = -0.5f;
        float maxPoint = 0.5f;

        Vector3[] vertices = {
            new Vector3 (minPoint, minPoint, minPoint),
            new Vector3 (maxPoint, minPoint, minPoint),
            new Vector3 (maxPoint, maxPoint, minPoint),
            new Vector3 (minPoint, maxPoint, minPoint),
            new Vector3 (minPoint, maxPoint, maxPoint),
            new Vector3 (maxPoint, maxPoint, maxPoint),
            new Vector3 (maxPoint, minPoint, maxPoint),
            new Vector3 (minPoint, minPoint, maxPoint),
        };

        int[] triangles = {
            0, 2, 1, //face front
			0, 3, 2,
            2, 3, 4, //face top
			2, 4, 5,
            1, 2, 5, //face right
			1, 5, 6,
            0, 7, 4, //face left
			0, 4, 3,
            5, 4, 7, //face back
			5, 7, 6,
            0, 6, 7, //face bottom
			0, 1, 6
        };

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.RecalculateNormals();

        Shader shader = Shader.Find("Quixel/FlatShadingGPU");
        Material cubeMat = new Material(shader);
        meshRenderer.material = cubeMat;
        meshRenderer.material.SetMatrix("_currentTRSMatrix" , Matrix4x4.TRS(transform.position,transform.rotation,transform.localScale));
    }


    private void OnRenderObject()
    {
        meshRenderer.material.SetVector("_Rotation", rotationSpeed);
        meshRenderer.material.SetVector("_Pivot", pivot);
        if (transform.parent != null)
            meshRenderer.material.SetMatrix("_ParentTRSMatrix", transform.parent.GetComponent<CubeGenerator>().getCurrentTRSMatrix());
    }

    public Matrix4x4 getCurrentTRSMatrix()
    {
        print(meshRenderer.material.GetMatrix("_currentTRSMatrix"));
        return meshRenderer.material.GetMatrix("_currentTRSMatrix");
    }
}
